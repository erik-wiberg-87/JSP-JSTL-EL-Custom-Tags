package se.club.beans;

public class Comment {
	String authorName;
	String message;
	boolean isMadeByAdmin;
	
	public Comment(String authorName, String message, boolean isMadeByAdmin) {
		this.authorName = authorName;
		this.message = message;
		this.isMadeByAdmin = isMadeByAdmin;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getMessage() {
		return message;
	}

	public boolean isMadeByAdmin() {
		return isMadeByAdmin;
	}

	public void setMadeByAdmin(boolean isMadeByAdmin) {
		this.isMadeByAdmin = isMadeByAdmin;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
