<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ attribute name="comments" required="true" type="java.util.List"%>

<div class="comment-list">
	<h4>${ comments.size() } comments for this post:</h4>
	
	<!-- 
		motsvarande for(Comment comment : comments) {
			// do stuff
		}
		
		 -->
	<c:forEach var="comment" items="${ comments }" >
		<%-- 
			skicka vår Comment som data till vår  
			custom tag "comment"
			--%>
		<t:comment comment="${ comment }"></t:comment>
	</c:forEach>

</div>
