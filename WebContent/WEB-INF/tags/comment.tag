<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="comment" required="true" type="se.club.beans.Comment"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<div class="comment">
	
	<%-- Vi kan anv�nda vissa functioner, t.ex. fn:containsIgnoreCase --%>
	<c:set var="commentHasSwearword" value="${ fn:containsIgnoreCase(comment.message, 'fuck') }" />
	
	<c:set var="fontWeight" value="${ comment.madeByAdmin ? 'bold' : 'normal' }"/>
	
	
	<%-- skriv bara ut kommentaren ifall den saknar ordet "fuck" --%>
	<c:if test="${ not commentHasSwearword }">
		<%-- Comment gets underline if the author is admin --%>
		<p style="font-weight: ${fontWeight}">${ comment.message }
			<span style="font-style: italic; color:blue"> - ${ comment.authorName }</span>
		</p>
	</c:if>
	<c:if test="${  commentHasSwearword}">
	 <p style="">[Automatically censored comment]<span style="font-style: italic; color:blue"> - ${ comment.authorName }</span></p>
	</c:if>
	
</div>