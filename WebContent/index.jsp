<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!-- ladda in så att vi "når" JSTL tags, via c:taggensNamn -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- ladda in så att vi "når" våra custom tags, via t:taggensNamn -->
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Comment page</title>
</head>
<body>

<h2>This is a POST telling the community something.. </h2>
<h4>Hello. Just wanted to say that the community is now open. Welcome!</h4>
<hr>

<!-- hämta bönan med tillgång till alla comments (List<Comment>)  -->
<jsp:useBean id="CommentsBean" class="se.club.beans.CommentsBean" scope="page" />
<!-- gör kommentarerna tillgängliga i en variabel 'comments' -->
<c:set var="comments" value="${ CommentsBean.comments }"></c:set>

<!-- Java EL (expression language) + JSTL (Java Standard Tag Library) -->
<c:if test="${ empty comments }">
	<%-- visas bara ifall comments är null eller empty  --%>
	<p>No comments yet!</p>
</c:if>

<c:if test="${ not empty comments }">
	<%--
		Skicka in listan List<Comment> som data till vår custom tag
		"comment-list". se WEB-INF/tags
	  --%>
	<t:comment-list comments="${ comments }" />
</c:if>

</body>
</html>