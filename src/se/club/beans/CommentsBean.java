package se.club.beans;

import java.util.ArrayList;
import java.util.List;

public class CommentsBean {
	List<Comment> comments;
	
	public CommentsBean() {
		comments = new ArrayList<>();
		comments.add(new Comment("Jahmal", "nice!", false));
		comments.add(new Comment("Alicia", "cool stuff!", false));
		comments.add(new Comment("Kalle", "duh, tell me something new!", false));
		comments.add(new Comment("Admin 1", "Ok Kalle, keep it positive in here!", true));
		comments.add(new Comment("Kalle", "Fuck you admin!", false));
		comments.add(new Comment("Admin 1", "Kalle, this is your first warning! 2 more..", true));
		comments.add(new Comment("Andr�", "That's it? Nothing more?!", false));
		comments.add(new Comment("Kalle", "What the fuck is this? I'm being censored?", false));
		comments.add(new Comment("Admin 2", "That's enough. Thread closed. Shut up", true));
	}
	
	public List<Comment> getComments() {
		System.out.println("Getting comments..");
		return comments;
	}
}
